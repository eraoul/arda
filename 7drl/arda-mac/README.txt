                   ~~~~~~~~~~~~~~~~~
                   Shadows Over Arda
                   ~~~~~~~~~~~~~~~~~
   
Author: Eric Nichols
Release Date: March 10, 2024
Code: https://gitlab.com/eraoul/arda

Introduction
------------
Shadows Over Arda is an ASCII (actually, unicode)-based
adventure game inspired by the rich lore of J.R.R.
Tolkien's Silmarillion, specifically focusing on the
ancient struggles between the Valar and Melkor.
Players will navigate the world of Arda, using their
wits and powers to shape the land, combat corruption,
and fulfill their destiny.

Running the Game
----------------
This is a temrinal-based game that needs to be run from a 
terminal window. See instructions below to get it running.
Note that Windows requires manual font setup first due to 
the Unicode characters included in the game.

Installation/Running Instructions (Windows or Mac)
------------
* Windows *

1. Font Installation:

Before running the game, install a good monospaced 
Unicode font. We recommend using GNU FreeFont included
in the game package in the "font" folder.

Install FreeMono.ttf by right-clicking the file and
selecting "Install for all users."

2. Open and size a terminal (IMPORTANT)

- Open a Windows terminal.
- Resize the terminal so it's large enough (119w x 38h).
If not large enough, the game will give you an error on start.

3. Set the Terminal Font:

Open your terminal settings and select GNU FreeFont
(FreeMono) as the default font. You may also want to
increase the font size for better visibility.

4. Running the Game:

Important: must run from the terminal. Double-clicking the 
exe file will result in a terminal that is too small.

Navigate to the "win" directory where the game is located.

Run arda.exe from the terminal to start your adventure in Arda.


* Mac *

1. Preparing Your Terminal:

The game is designed to run in the Mac Terminal using
the default font, Andale Mono, which should work fine.

If desired, you can also install and use GNU FreeFont.
Install FreeMono.ttf by double-clicking the file. Then,
set it as the default font in your Terminal preferences.

2. Running the Game:

Open a terminal window and navigate to the "mac" directory 
containing the game.

Run the script run_arda.sh to set up the terminal for
the game and start your journey.

The script ensures that terminal settings are correctly
configured for color support and compatibility with the
"curses" library.


Enjoy Your Adventure
--------------------
Embark on your quest in the world of Arda, and may your
deeds be sung of in the halls of Valinor!

