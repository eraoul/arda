import curses
from game import Game

MIN_HEIGHT = 38
MIN_WIDTH = 119


def main(stdscr):

    stdscr.clear()
    height, width = stdscr.getmaxyx()

    if height < MIN_HEIGHT or width < MIN_WIDTH:
        msg = f"Please resize the terminal to at least {MIN_WIDTH} wide x {MIN_HEIGHT} high and restart the game."
        stdscr.addstr(0, 0, msg)
        stdscr.addstr(2, 0, 'Press any key to quit', curses.A_DIM)
        curses.curs_set(0)  # Hide cursor
        stdscr.refresh()
        stdscr.getch()  # Wait for user input before exiting
        return
    # stdscr.addstr(0, 0, 'Curent screen size: {}x{}'.format(width, height))
    # stdscr.getch()

    game = Game(stdscr)
    game.start_game(stdscr)

    curses.nocbreak()
    stdscr.keypad(0)
    curses.echo()
    curses.endwin()


if __name__ == "__main__":

    # if on mac, resize the terminal
    import os

    if os.name == 'posix':
        os.system(f'resize -s {MIN_HEIGHT} {MIN_WIDTH}')

    curses.wrapper(main)
