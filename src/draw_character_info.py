import curses

from display import center, CHAR_INFO_WIDTH
from abilities import ABILITY_MAP

# from util import log


def draw_character_info(win, character, is_harmonious):
    win.clear()
    win.box()

    char_lines = [
        center(character.name, CHAR_INFO_WIDTH),
        center(character.description, CHAR_INFO_WIDTH),
        "",
    ]

    ability_lines = [
        "Abilities: ",
        *[f'{i+1}. ' + ABILITY_MAP[ability]
          for i, ability in enumerate(character.abilities)],
        "-" * CHAR_INFO_WIDTH,
    ]

    stats_lines = [
        f"Level: {character.level}",
        # f"XP: {character.xp}",
        # f"Health: {character.health}",
        # f"Harmony: {character.harmony_points}",
        "-" * CHAR_INFO_WIDTH,
    ]

    inventory_lines = [
        "Inventory:",
        *[f'• {item}' for item in character.inventory],
        # "-" * CHAR_INFO_WIDTH,
    ]

    lines = char_lines + ability_lines
    lines2 = stats_lines + inventory_lines

    # lines = [
    #     center(character.name, CHAR_INFO_WIDTH),
    #     center(character.description, CHAR_INFO_WIDTH),
    #     "",
    #     "Abilities: ",
    #     *[f'{i+1}. ' + ABILITY_MAP[ability]
    #       for i, ability in enumerate(character.abilities)],
    #     "-" * CHAR_INFO_WIDTH,
    # ] + [f"{stat}: {value}" for stat, value in character.stats.items()] + [
    #     f"Next level at XP: {character.xp_required_for_next_level()}",
    #     "-" * CHAR_INFO_WIDTH,
    #     "Inventory:",
    # ] + [f'• {item}' for item in character.inventory]

    # Draw character and ability info at top.
    for i, line in enumerate(lines):
        win.addstr(i + 1, 1, line)

    cur_row = i + 2

    # Draw progress bars for Harmony Points, Health, and XP.
    rows, cols = win.getmaxyx()

    draw_progress(win, "Harmony", character.harmony_points,
                  100, cur_row, 1, cols - 2)
    cur_row += 1

    draw_progress(win, "Health", character.health,
                  character.max_health, cur_row, 1, cols - 2)
    cur_row += 1

    draw_progress(win, "XP", character.xp,
                  character.xp_required_for_next_level(), cur_row, 1, cols - 2, color_pair=6)
    cur_row += 1

    # Draw other stats and inventory at bottom.
    for i, line in enumerate(lines2):
        win.addstr(cur_row + i, 1, line)

    # Show bottom help messages
    if is_harmonious:
        win.addstr(
            rows-4, 1, center("Press h to proclaim this region as harmonious", cols-2), curses.color_pair(1))

    txt = 'Press ? for help'
    win.addstr(rows-2, 1, center(txt, cols-2))

    # win.refresh()


def draw_progress(window, stat_name, cur_points, max_points, start_y, start_x, width, color_pair=None):
    fill_percentage = int((cur_points / max_points) * 100)

    # Determine color based on given color pair OR fill level
    if color_pair:
        color = curses.color_pair(color_pair)
    elif fill_percentage >= 75:
        color = curses.color_pair(6)  # Green
    elif fill_percentage >= 40:
        color = curses.color_pair(1)  # Yellow
    else:
        color = curses.color_pair(7)  # Red

    # Prepare the progress bar and info text
    info_text = f"{stat_name}: {cur_points}/{max_points} "
    len_txt = len(info_text)
    bar_width = width - len_txt - 3  # padding: 2 sides of bar and 1 space after

    # Calculate fill level in characters
    fill = int(bar_width * cur_points / max_points)

    bar = '[' + ('#' * fill) + (' ' * (bar_width - fill)) + ']'

    # Add the info text without color
    window.addstr(start_y, start_x, info_text)

    # Add the progress bar with color
    window.addstr(start_y, start_x + len(info_text) + 1, bar, color)
