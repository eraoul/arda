from collections import defaultdict
import random
import time

from melkor import MELKOR_JUNK
from display import DISPLAY

TERRAIN_WEIGHTS = defaultdict(int)

weights = {
    # Harmonious terrain types
    'ocean': 1,
    'water': 2,
    'marsh': 1,
    'valley': 1,
    'forest': 2,
    'hill1': 1,
    'hill2': 1,
    'mountain': 1,
    'light_tree': 1,
}

for terrain, weight in weights.items():
    TERRAIN_WEIGHTS[DISPLAY[terrain]] = weight

for c in MELKOR_JUNK:
    TERRAIN_WEIGHTS[c] = -17  # Chaotic elements introduced by Melkor


def compute_harmony_score(map_width, map_height, game_map):
    score = 0
    # Offsets for the four direct neighbors
    neighbors = [(0, -1), (0, 1), (-1, 0), (1, 0)]

    for y in range(map_height):
        for x in range(map_width):
            terrain_type = game_map[y][x]

            for dx, dy in neighbors:
                nx, ny = x + dx, y + dy
                if 0 <= nx < map_width and 0 <= ny < map_height:  # Check bounds
                    neighbor_type = game_map[ny][nx]
                    if terrain_type == neighbor_type:
                        # Increase score for adjacent cells of the same type
                        score += TERRAIN_WEIGHTS[terrain_type]
                    # else:
                    #     # Optional: Decrease score for adjacent cells of different types
                    #     # This line can be adjusted based on desired gameplay dynamics
                    #     score -= TERRAIN_WEIGHTS[neighbor_type]

    return max(0, score)


animate_sleep_time = 0.03


def animate_harmonious(game_map, map_width_full, map_height_full, draw_fn):
    # get all melkor junk tile locations.
    junk_tiles = []
    for y in range(map_height_full):
        for x in range(map_width_full):
            if game_map[y][x] in MELKOR_JUNK:
                junk_tiles.append((x, y))

    random.shuffle(junk_tiles)
    while junk_tiles:
        x, y = junk_tiles.pop(0)
        game_map[y][x] = DISPLAY['valley']
        draw_fn()
        time.sleep(animate_sleep_time)
