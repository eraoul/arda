import curses


def confirm_quit():
    # Create a new window for the confirmation popup
    confirm_win = curses.newwin(
        3, 40, (curses.LINES - 3) // 2, (curses.COLS - 40) // 2)
    confirm_win.box()
    confirm_win.addstr(1, 1, "Are you sure you want to quit? (Y/N)")

    # Display the popup and wait for a response
    confirm_win.refresh()
    key = confirm_win.getch()

    # Close the popup window
    confirm_win.clear()
    confirm_win.refresh()

    # Return True if the user confirms quitting, False otherwise
    return key in [ord('y'), ord('Y')]
