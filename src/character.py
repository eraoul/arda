class Character:
    def __init__(self, msg_fn, popup_msg_fn):
        self.msg_fn = msg_fn
        self.popup_msg_fn = popup_msg_fn

        self.name = ""
        self.description = ""
        self.abilities = []
        self.levelable_ability = ''
        self.stats = {}
        self.items = []
        self.inventory = []
        self.symbol = ""
        self.color_pair = 0
        self.water_only = False
        self.can_swim = False
        self.can_climb = False

    # character.stats = {"Level": 100, "XP": 0,
    #                    "Max Health": 100, "Health": 100, "Harmony Points": 50}

    @property
    def harmony_points(self):
        return self.stats['Harmony Points']

    @property
    def level(self):
        return self.stats['Level']

    @property
    def health(self):
        return self.stats['Health']

    @health.setter
    def health(self, amount):
        self.stats['Health'] = amount

    @property
    def max_health(self):
        return self.stats['Max Health']

    @property
    def xp(self):
        return self.stats['XP']

    def increase_level(self):
        self.stats['Level'] += 1
        self.increase_max_health(20)
        self.health = self.max_health

        # Check for new abilities.
        if self.level % 5 == 0 and self.level <= 20:
            core_ability = self.levelable_ability
            new_ability = f'{core_ability}{1 + self.level // 5}'
            self.abilities.append(new_ability)
            self.popup_msg_fn(f'{self.name} learned {new_ability}.')
        else:
            self.popup_msg_fn(f'{self.name} is now level {self.level}.')

    def increase_health(self, amount):
        self.health = min(self.health + amount, self.max_health)

    def increase_max_health(self, amount):
        self.stats['Max Health'] += amount

    def xp_required_for_next_level(self):
        """XP required to reach next level, based on current level."""
        return 100 + 100 * int(self.stats['Level'] ** 1.1)

    def increase_xp(self, amount):
        self.stats['XP'] += amount

        xp_required = self.xp_required_for_next_level()
        if self.stats['XP'] >= xp_required:
            self.stats['XP'] -= xp_required
            self.increase_level()

    def increase_harmony(self, amount):
        self.stats['Harmony Points'] += amount
        # limit to 100 max.
        self.stats['Harmony Points'] = min(self.stats['Harmony Points'], 100)

    def cleaned_corruption(self):
        """Call this when the character cleans one tile of Melkor corruption."""
        self.increase_xp(10)
        self.increase_harmony(1)
        self.increase_health(1)

    def spend_harmony(self, amount):
        self.stats['Harmony Points'] -= int(amount)
