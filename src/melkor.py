import math
import random
import time

from util import random_sample
from config import OCEAN_BORDER, VOID_BORDER, TOTAL_BORDER


animate_sleep_time = 0.5
JUNK_PROBABILITY = 0.7

MELKOR_JUNK = ['▘', '▒', '▓', '▚', '▞', '▟']

MELKOR_MESSAGES = [
    "The earth trembles as Melkor's malice spreads through Arda.",
    "Shadow and ruin spread; Melkor's wrath is unleashed!",
    "Melkor attacks! A foul corruption seeps into the land, twisting it beyond recognition.",
    "Melkor's influence grows, as darkness swallows the landscape.",
    "The world groans under the weight of Melkor's destruction."
]


def run_melkor_destruction(game_map, map_width, map_height, level, draw_fn, update_harmony_score_fn):
    # choose # craters based on level.
    num_craters = 5 + level // 4

    for _ in range(num_craters):  # Simulate 5 points of destruction
        x, y = choose_destruction_point(map_width, map_height)
        create_crater(x, y, game_map, map_width, map_height, level)
        update_harmony_score_fn()
        draw_fn(refresh=True)  # Refresh the game map to show changes
        time.sleep(animate_sleep_time)  # Pause briefly to simulate animation


def choose_destruction_point(map_width, map_height):

    x = random.randint(0, map_width - TOTAL_BORDER - 1)
    y = random.randint(0, map_height - TOTAL_BORDER - 1)
    return x+TOTAL_BORDER, y+TOTAL_BORDER


def create_crater(x, y, game_map, map_width, map_height, level):
    # choose crater size based on level plus a random component.
    radius = random.randint(1, 3) + level // 2
    for i in range(-radius, radius + 1):
        for j in range(-radius, radius + 1):
            if i**2 + j**2 <= radius**2:
                cx = x + i
                cy = y + j
                if (cx >= TOTAL_BORDER and cx < TOTAL_BORDER + map_width and
                        cy >= TOTAL_BORDER and cy < TOTAL_BORDER + map_height):
                    # one more random sample: only generate with probability JUNK_PROBABILITy
                    if random.random() > JUNK_PROBABILITY:
                        continue

                    # Check if within bounds and then transform the tile
                    c = random_sample(MELKOR_JUNK)
                    game_map[cy][cx] = c


def melkor_corrupted(game_map, x, y):
    return game_map[y][x] in MELKOR_JUNK


MELKOR_HEALTH_RADIUS = 4  # The radius within which health is affected by corruption
CORRUPTION_TILE = 'X'  # Assuming 'X' marks corruption tiles in game_map

MELKOR_HEALTH_MESSAGES = [
    "Near rot, lost {health_decline} health.",
    "Melkor's blight costs {health_decline} health.",
    "Near Melkor's blight, {health_decline} life ebbed.",
    "Close foulness drains {health_decline} life.",
    "Melkor's shadow near, lost {health_decline} life."
]


def reduce_health_near_corruption(character, game_map, map_width_full, map_height_full, character_x, character_y, msg_fn, blink_fn):
    min_sq_distance = float('inf')
    MAX_HEALTH_DECLINE = 8  # Max health decline at the closest point of corruption

    if character.water_only:
        MAX_HEALTH_DECLINE /= 2     # reduce effect of Melkor if in the water

    # Iterate over the game map to find the nearest corruption tile
    for y in range(max(0, character_y - MELKOR_HEALTH_RADIUS),
                   min(map_height_full - 1, character_y + MELKOR_HEALTH_RADIUS + 1)):
        for x in range(max(0, character_x - MELKOR_HEALTH_RADIUS),
                       min(map_width_full - 1, character_x + MELKOR_HEALTH_RADIUS + 1)):
            if melkor_corrupted(game_map, x, y):
                distance = (character_x - x) ** 2 + (character_y - y) ** 2
                min_sq_distance = min(min_sq_distance, distance)

    # Calculate health decline based on proximity to the nearest corruption tile
    if min_sq_distance <= MELKOR_HEALTH_RADIUS**2:
        if min_sq_distance == 0:
            health_decline = MAX_HEALTH_DECLINE
        else:
            # Inversely proportional to distance squared
            health_decline = math.ceil(
                MAX_HEALTH_DECLINE / min_sq_distance**0.5)
        # Ensure health doesn't drop below 0
        character.health = max(character.health - health_decline, 0)

        msg_fn(random.choice(MELKOR_HEALTH_MESSAGES).format(
            health_decline=health_decline))

        # blink the character to show damage.
        blink_fn()
