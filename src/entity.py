import curses


class Entity:
    def __init__(self, x, y, symbol, color_pair):
        self.x = x
        self.y = y
        self.symbol = symbol
        self.color_pair = color_pair

# class Ocean:
#     def __init__(self, x, y):
#         super().__init__(x, y, '~', curses.COLOR_WHITE)
