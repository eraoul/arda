import curses
from display import DISPLAY, HEIGHT_TO_TERRAIN, CHAR_TO_COLOR_PAIR


def show_help_screen():
    # Adjust size and position as needed
    help_win = curses.newwin(20, 60, 5, 10)
    help_win.box()
    help_win.addstr(1, 1, "Keyboard Commands", curses.A_BOLD)
    commands = [
        "Arrow keys - Move",
        # "p - Pick up item",
        # "u - Use item (followed by item number)",
        "1,2,3,4,5 - Sing/Use ability 1,2,3,4,5",
        "h - Proclaim this region as harmonious",
        'SPACE - pass a turn',
        "? - Show this help screen",
        "q - Quit game",
    ]

    for i, command in enumerate(commands):
        help_win.addstr(i + 2, 1, command)
    row = i + 4

    for item in HEIGHT_TO_TERRAIN.values():
        item_txt = item
        if item == 'hill1':
            item_txt = 'small hill'
        elif item == 'hill2':
            item_txt = 'large hill'
        help_win.addstr(
            row, 1, DISPLAY[item], curses.color_pair(CHAR_TO_COLOR_PAIR[DISPLAY[item]]))
        help_win.addstr(row, 3, f"- {item_txt}")
        row += 1

    help_win.refresh()
    help_win.getch()  # Wait for any key to close the help screen
    help_win.clear()
