import curses
import time

from display import CHAR_TO_COLOR_PAIR
from draw_character_info import draw_character_info
from draw_messages import draw_messages

BLINK_DELAY = 0.05


def draw(game):

    # Normalize harmony score.
    harmony_score = int(game.harmony_score / game.initial_harmony_score * 100)

    game.stdscr.clear()
    game.stdscr.addstr(
        0, 0, f"{game.game_name} - Level {game.level} - Turns: {game.turns} - Harmony: {harmony_score}")
    for y, row in enumerate(game.game_map):
        for x, char in enumerate(row):
            if (y, x) == (game.player_pos[0], game.player_pos[1]):
                # Draw player with color
                game.stdscr.addstr(
                    y+1, x, game.character.symbol, curses.color_pair(game.character.color_pair))
            else:
                color_pair = CHAR_TO_COLOR_PAIR[char]
                # log(f'{char}, {color_pair}, {y+1}, {x}, {curses.color_pair(color_pair)}')
                game.stdscr.addstr(
                    y+1, x, char, curses.color_pair(color_pair))
                # game.stdscr.refresh()
                # time.sleep(0.001)
    # game.stdscr.refresh()

    # Character info and msg windows.
    draw_character_info(game.char_info_win, game.character,
                        game.can_declare_harmonious())
    draw_messages(game.msg_win, game.messages)


def blink_character(stdscr, character, player_pos):
    # Draw player with highlighted backgound color
    stdscr.addstr(
        player_pos[0] + 1, player_pos[1], character.symbol, curses.color_pair(31))
    stdscr.refresh()
    # wait
    time.sleep(BLINK_DELAY)

    # redraw normal.
    stdscr.addstr(
        player_pos[0] + 1, player_pos[1], character.symbol, curses.color_pair(character.color_pair))
    stdscr.refresh()
