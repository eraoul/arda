import curses

from util import random_sample
from melkor import MELKOR_JUNK


CHAR_INFO_WIDTH = 50

DISPLAY = {'ocean': '∿', 'water': '~', 'void': '·', 'wall': '█',
           'mountain': '⌃', 'hill1': '△', 'hill2': '▲', 'valley': '.',  # ⛰
           'marsh': '░', 'forest': '^', 'light_tree': '☼'}   # 🌲


def is_water(tile):
    return tile == DISPLAY['water'] or tile == DISPLAY['ocean']


def is_mountain(tile):
    return tile == DISPLAY['mountain']


HEIGHT_TO_TERRAIN = {
    0: 'ocean',
    1: 'water',
    2: 'marsh',
    3: 'valley',
    4: 'forest',
    5: 'hill1',
    6: 'hill2',
    7: 'mountain',
}

MIN_TERRAIN_HEIGHT = 0
MAX_TERRAIN_HEIGHT = max(HEIGHT_TO_TERRAIN.keys())

MIN_LAND_HEIGHT_SCALED = 2.0 / MAX_TERRAIN_HEIGHT
MAX_LAND_HEIGHT_SCALED = 1.0

CHAR_TO_COLOR_PAIR = {DISPLAY['ocean']: 2,
                      DISPLAY['water']: 12,
                      DISPLAY['void']: 3,
                      DISPLAY['wall']: 5,
                      DISPLAY['mountain']: 11,
                      DISPLAY['hill1']: 5,
                      DISPLAY['hill2']: 5,
                      DISPLAY['valley']: 13,
                      DISPLAY['marsh']: 3,
                      DISPLAY['forest']: 6,
                      DISPLAY['light_tree']: 14,
                      ' ': 4}

for c in MELKOR_JUNK:
    CHAR_TO_COLOR_PAIR[c] = random_sample([20, 21, 22, 23, 24])


def initialize_colors():
    if not curses.has_colors():
        print('error! no color support')
        exit(0)

    curses.start_color()

    # curses.init_pair(pair_number, foreground_color, background_color)
    curses.init_pair(1, curses.COLOR_YELLOW,
                     curses.COLOR_BLACK)

    curses.init_pair(2, curses.COLOR_BLUE,
                     curses.COLOR_BLACK)

    curses.init_pair(3, curses.COLOR_MAGENTA,
                     curses.COLOR_BLACK)

    curses.init_pair(4, curses.COLOR_WHITE,
                     curses.COLOR_BLACK)

    curses.init_color(10, 543, 270, 74)   # color #, R,G,B (0-1000)
    curses.init_pair(5, 10, curses.COLOR_BLACK)  # brown

    curses.init_pair(6, curses.COLOR_GREEN,
                     curses.COLOR_BLACK)

    curses.init_pair(7, curses.COLOR_RED,
                     curses.COLOR_BLACK)

    # Custom terrain
    curses.init_color(11, 343, 170, 0)  # brown
    curses.init_color(12, 800, 800, 700)    # dirty white
    # curses.init_pair(11, 11, 12)  # mountain brown on brown
    curses.init_pair(11, 12, 11)  # mountain brown on brown

    curses.init_color(13, 0, 0, 300)  # dark blue
    curses.init_pair(12, curses.COLOR_BLUE, 13)  # blue on dark blue

    curses.init_color(14, 600, 700, 200)  # light green/yellow
    curses.init_pair(13, 14, curses.COLOR_BLACK)  # light green on black

    curses.init_color(15, 1000 * 251 // 255, 1000, 0)  # FBFF00
    curses.init_color(16, 1000, 1000 * 136 // 255, 0)     # FF8800
    # FBFF00 on FF8800 ; yellow on orange
    curses.init_pair(14, 15, 16)

    # Melkor junk
    curses.init_color(20, 543, 10, 20)
    curses.init_color(21, 400, 100, 20)
    curses.init_color(22, 400, 20, 400)
    curses.init_color(23, 900, 20, 300)
    curses.init_color(24, 700, 20, 200)
    curses.init_pair(20, 20, curses.COLOR_BLACK)
    curses.init_pair(21, 21, curses.COLOR_RED)
    curses.init_pair(22, 22, curses.COLOR_BLACK)
    curses.init_pair(23, 23, curses.COLOR_RED)
    curses.init_pair(24, 24, curses.COLOR_BLACK)

    # Initialize more color pairs for other uses as needed
    curses.init_pair(30, curses.COLOR_WHITE,
                     curses.COLOR_YELLOW)  # white on yellow

    # yellow on red, then you're dead
    curses.init_pair(31, curses.COLOR_YELLOW, curses.COLOR_RED)


def center(text, max_width):
    return ' ' * int((max_width - len(text)) / 2) + text
