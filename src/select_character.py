from copy import deepcopy
import curses

from abilities import ABILITY_MAP
from character import Character

ITEM_DESCRIPTIONS = {
    'wind-spear': 'Spear of the Soaring Wind',
    'sky-cloak': 'Mantle of the Azure Sky',
    'star-bow': "Bow of Stars' Lament",
    'light-veil': 'Veil of Ilmen',
    'water-horn': "Ulmo's Horn",
    'rain-cloak': 'Cloak of Mist and Rain',
    'mountain-hammer': 'Hammer of the Undermountain',
    'stone-belt': "Belt of the World's Core",
    'growth-staff': 'Staff of Blossoming',
    'renewal-garland': 'Garland of Renewal',
}

CHARACTERS = {
    '1': {'name': 'Manwë', 'symbol': 'M', 'description': "King of the Valar, lord of winds and air.",
          #   'abilities': ['weather', 'wind']},
          'abilities': ['wind',],
          'levelable_ability': 'wind',
          'items': ['wind-spear', 'sky-cloak']},
    '2': {'name': 'Varda', 'symbol': 'V', 'description': "Queen of the Stars, illuminator of darkness.",
          #   'abilities': ['light', 'barriers']},
          'abilities': ['light',],
          'levelable_ability': 'light',
          'items': ['star-bow', 'light-veil']},
    '3': {'name': 'Ulmo', 'symbol': 'U', 'description': "Lord of Waters, ruler of seas and oceans.",
          #   'abilities': ['water']},
          'abilities': ['water',],
          'levelable_ability': 'water',
          'items': ['water-horn', 'rain-cloak']},
    '4': {'name': 'Aulë', 'symbol': 'A', 'description': "The Smith, master of the substance of Arda.",
          #   'abilities': ['forge', 'earth']},
          'abilities': ['smooth', 'earth'],
          'levelable_ability': 'earth',
          'items': ['mountain-hammer', 'stone-belt']},
    '5': {'name': 'Yavanna', 'symbol': 'Y', 'description': "Giver of Fruits, goddess of all things that grow.",
          #   'abilities': ['growth', 'animals']},
          'abilities': ['growth',],
          'levelable_ability': 'growth',
          'items': ['growth-staff', 'renewal-garland']},
}


def show_character_selection(stdscr, game):
    stdscr.clear()
    stdscr.addstr(2, 10, "Choose your character:", curses.A_BOLD)
    y_offset = 3
    for idx, char_info in CHARACTERS.items():
        abilities = '; '.join(
            [ABILITY_MAP[ability] for ability in char_info['abilities']])
        stdscr.addstr(int(
            idx) + y_offset, 10, f"{idx}: {char_info['name']} - {char_info['description']}")
        stdscr.addstr(int(idx) + y_offset + 1, 12,
                      f"Abilities: {abilities}", curses.A_DIM)
        y_offset += 2
    stdscr.refresh()
    char = '0'
    while char not in CHARACTERS:
        char = stdscr.getkey()
    # If we don't copy, it's like a Roguelite and abilities stay in new games.
    char_info = deepcopy(CHARACTERS[char])

    game.player_color_pair = 1

    character = Character(game.msg, game.popup_msg)
    character.symbol = char_info['symbol']
    character.name = char_info['name']
    character.description = char_info['description']
    character.abilities = char_info['abilities']
    character.levelable_ability = char_info['levelable_ability']
    character.stats = {"Level": 1, "XP": 0,
                       "Max Health": 100, "Health": 100, "Harmony Points": 50}
    character.items = char_info['items']
    character.inventory = [ITEM_DESCRIPTIONS[item] for item in character.items]
    # character.inventory = ["Sword of the Valar", "Shield of the Valar"]

    if character.name == 'Ulmo':  # Ulmo
        game.init_player_pos = [1, 21]
        game.player_pos = [1, 21]
        character.water_only = True
        character.can_swim = True

    if character.name == 'Aulë':
        character.can_climb = True

    if character.name == 'Manwë':
        # technicaly he flies, but this simulates flight.
        character.can_climb = True
        character.can_swim = True

    game.msg(
        f"Character selected: {character.name}")

    return character
