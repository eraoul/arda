from copy import deepcopy
import curses
import random
# import time

import numpy as np

from abilities import apply_tree_effects, earth_ability, growth_ability, light_ability, smooth_ability, water_ability, wind_ability
from config import TOTAL_BORDER
from display import CHAR_INFO_WIDTH, DISPLAY, initialize_colors, is_water, is_mountain
from draw import draw as draw_main
from draw import blink_character as draw_blink_character
from draw_intro import draw_intro
from draw_messages import draw_messages
from harmony import animate_harmonious, compute_harmony_score
from help import show_help_screen
from map import generate_terrain_and_elevations  # generate_map
from melkor import run_melkor_destruction, MELKOR_MESSAGES, reduce_health_near_corruption
from quit import confirm_quit
from select_character import show_character_selection
from title import show_title_screen
from util import random_sample, log

KEY_TO_DIRECTION_INT = {
    curses.KEY_UP: 0,
    curses.KEY_RIGHT: 1,
    curses.KEY_DOWN: 2,
    curses.KEY_LEFT: 3,
}


class Game:
    def __init__(self, stdscr):
        log('initializing game')
        self.stdscr = stdscr
        self.msg_win = None

        # initialize_colors()
        self.map_height = 25
        self.map_width = 60

        self.map_height_full = self.map_height + 2 * TOTAL_BORDER
        self.map_width_full = self.map_width + 2 * TOTAL_BORDER

        self.map_elevations = None  # 2D array

        self.turns = 0
        self.level = 0
        self.game_name = "Shadows Over Arda"
        self.trees = []

        self.player_char = '@'  # Default player character symbol
        self.character = None
        self.init_player_pos = [11, 21]   # in order [y, x]
        self.player_pos = [11, 21]   # in order [y, x]

        curses.curs_set(0)  # Hide cursor
        self.stdscr.nodelay(False)  # Wait for key press
        self.stdscr.clear()  # Clear screen before starting

        self.uncorrupted_map = []
        # map with ints indicating how many turns left until water recedes
        self.flood_time_map = []
        self.harmony_score = 0
        self.initial_harmony_score = 0

        self.messages = []

    def draw(self, refresh=True):
        draw_main(self)
        self.refresh()

    def refresh(self):
        self.stdscr.refresh()
        self.msg_win.refresh()
        self.char_info_win.refresh()

    def msg(self, msg):
        self.messages.append(msg)
        if self.msg_win:
            draw_messages(self.msg_win, self.messages)

    def popup_msg(self, msg, msg2=''):
        # make a new popup window and display the message.
        # wait for keypress to clear.
        popup_win = curses.newwin(
            5, 40, (curses.LINES - 3) // 2, (curses.COLS - 40) // 2)
        popup_win.box()
        popup_win.addstr(1, 1, msg, curses.color_pair(1))
        if msg2:
            popup_win.addstr(2, 1, msg2, curses.color_pair(1))
        popup_win.addstr(3, 1, 'Press SPACE to continue...')

        # Display the popup and wait for a response
        popup_win.refresh()

        while True:
            if popup_win.getch() == ord(' '):
                break

        # Close the popup window
        popup_win.clear()
        popup_win.refresh()

    def move_player(self, direction):
        dy, dx = 0, 0
        if direction == curses.KEY_UP:
            dy = -1
        elif direction == curses.KEY_DOWN:
            dy = 1
        elif direction == curses.KEY_LEFT:
            dx = -1
        elif direction == curses.KEY_RIGHT:
            dx = 1
        new_y = self.player_pos[0] + dy
        new_x = self.player_pos[1] + dx
        tile = self.game_map[new_y][new_x]
        if tile == DISPLAY['wall']:
            self.msg('You hit a wall!')
            return
        if tile == DISPLAY['ocean'] and not self.character.can_swim:
            self.msg(f"{self.character.name} can't swim in the ocean!")
            return
        if tile == DISPLAY['water'] and not self.character.can_swim:
            self.msg(f"{self.character.name} can't swim!")
            return
        if tile == DISPLAY['mountain'] and not self.character.can_climb:
            self.msg(f"{self.character.name} can't climb mountains!")
            return
        if tile == DISPLAY['void']:
            self.msg(f"{self.character.name} can't go through the void!")
            return
        if self.character.water_only and tile != DISPLAY['ocean'] and tile != DISPLAY['water']:
            self.msg(f"{self.character.name} prefers to stay in the water.")
            return

        self.player_pos = [new_y, new_x]

    def update_harmony_score(self):
        self.harmony_score = compute_harmony_score(
            self.map_width_full, self.map_height_full, self.game_map)

    def can_declare_harmonious(self):
        return self.turns >= 50 and self.harmony_score >= self.initial_harmony_score * .90  # 0.95

    def blink_character(self):
        draw_blink_character(self.stdscr, self.character, self.player_pos)

    def game_loop(self):
        # self.draw()
        next_melkor = 10

        # direction: 0 = up, 1 = right, 2 = down, 3 = left
        last_direction = 1
        while True:
            cur_y, cur_x = self.player_pos

            if self.turns == next_melkor:
                self.msg(random_sample(MELKOR_MESSAGES))
                run_melkor_destruction(
                    self.game_map, self.map_width, self.map_height, self.level, self.draw, self.update_harmony_score)
                self.turns += 1  # otherwise if the user presses an invalid key we end up re-running melkor
                # choose next melkor time. At higher levels, space out further since destruction is worse.
                next_melkor_base = 300 + self.level//4 * 100
                next_melkor = self.turns + random.randint(
                    next_melkor_base, next_melkor_base + 100)

            curses.flushinp()
            key = self.stdscr.getch()
            if key in [curses.KEY_UP, curses.KEY_DOWN, curses.KEY_LEFT, curses.KEY_RIGHT]:
                self.move_player(key)
                self.draw()
                self.turns += 1
                last_direction = KEY_TO_DIRECTION_INT[key]
            if key == ord('?'):  # Help screen
                show_help_screen()
                self.draw()
                # self.stdscr.refresh()
            elif key == ord('q'):  # Quit game
                if confirm_quit():
                    break
                self.draw()
            elif ord('1') <= key <= ord('9'):  # Ability 1, 2, 3, or 4 etc.
                n = key - ord('1')
                if n + 1 > len(self.character.abilities):
                    continue
                ability_key = self.character.abilities[n]
                self.msg(f'Using ability {ability_key}...')
                if 'water' in ability_key:
                    water_ability(self.character, ability_key, cur_x, cur_y, self.game_map, self.map_elevations,
                                  self.flood_time_map,
                                  self.map_width, self.map_height,
                                  self.map_width_full, self.map_height_full, self.msg, self.draw)
                elif 'earth' in ability_key:
                    earth_ability(self.character, ability_key, cur_x, cur_y, last_direction, self.game_map, self.map_elevations,
                                  self.map_width, self.map_height,
                                  self.map_width_full, self.map_height_full,
                                  self.river_sources,
                                  self.msg, self.draw)
                elif 'smooth' in ability_key:
                    smooth_ability(self.character, ability_key, cur_x, cur_y, self.game_map, self.map_elevations,
                                   self.map_width, self.map_height,
                                   self.map_width_full, self.map_height_full,
                                   self.river_sources,
                                   self.msg, self.draw)
                elif 'growth' in ability_key:
                    growth_ability(self.character, ability_key, cur_x, cur_y, self.game_map,
                                   self.map_width, self.map_height,
                                   self.map_width_full, self.map_height_full,
                                   #    self.river_sources,
                                   self.msg, self.draw)
                elif 'wind' in ability_key:
                    wind_ability(self.character, ability_key, cur_x, cur_y, last_direction, self.game_map, self.uncorrupted_map,
                                 self.map_width, self.map_height,
                                 self.map_width_full, self.map_height_full,
                                 #  self.river_sources,
                                 self.msg, self.draw)
                elif 'light' in ability_key:
                    light_ability(self.character, ability_key, cur_x, cur_y, self.game_map, self.trees,
                                  #   self.map_width, self.map_height,
                                  #   self.map_width_full, self.map_height_full,
                                  #   self.river_sources,
                                  self.msg, self.draw)

                self.update_harmony_score()
                self.draw()
                self.turns += 1
            elif key == ord('h'):  # New level
                if self.turns < 50:
                    self.msg('Not enough turns to declare this area harmonious.')
                    continue
                if not self.can_declare_harmonious():
                    self.msg(
                        'Not enough harmony to proclaim this area harmonious.')
                    continue
                self.msg("In these lands, harmony is ordained by the Valar.")

                animate_harmonious(self.game_map, self.map_width_full, self.map_height_full,
                                   self.draw)

                self.msg('Unto another region of Arda doth your journey lead.')
                won_game = self.new_level()
                if won_game:
                    return True
                next_melkor = 10
                self.draw()
            # space key
            elif key == ord(' '):
                # pass. Do nothing.
                self.turns += 1

            # Update flood map.
            if np.any(self.flood_time_map != 0):
                # Find indices where the value is 1.
                # In all such cases, restore the map before decrementing.
                ys, xs = np.where(self.flood_time_map == 1)

                # Combine the indices into pairs
                for x, y in zip(xs, ys):
                    self.game_map[y][x] = self.uncorrupted_map[y][x]

                # decrement all cells, with floor of 0.
                self.flood_time_map -= 1
                self.flood_time_map = np.maximum(self.flood_time_map, 0)

            # Regen harmony points over time.
            if self.turns % 10 == 0:
                self.character.increase_harmony(1)

            # Regen health points over time:
            if self.turns % 10 == 0:
                self.character.increase_health(1)

            # Remove health due to corruption.
            reduce_health_near_corruption(
                self.character, self.game_map, self.map_width_full, self.map_height_full,
                cur_x, cur_y, self.msg, self.blink_character)

            # Apply tree effects.
            apply_tree_effects(self.character, self.turns, cur_x, cur_y, self.game_map, self.trees,
                               self.map_width, self.map_height, self.map_width_full, self.map_height_full,
                               self.uncorrupted_map, self.draw, self.msg)
            self.update_harmony_score()

            # Check for (perma)death.
            if self.character.health <= 0:
                self.popup_msg(
                    f'{self.character.name} died. World level {self.level}.',
                    'Game Over.')
                return True

            # Finally, draw the screen.
            self.refresh()

    def new_level(self):
        if self.level == 20:
            self.popup_msg(f'{self.character.name} has succeeded in clearing 20 regions of Arda!',
                           'Congratulations! Game Over.')
            return True

        self.level += 1
        self.turns = 0
        self.character.increase_harmony(50)
        self.character.increase_health(50)
        self.game_map, self.map_elevations, self.river_sources = generate_terrain_and_elevations(
            self.map_width_full, self.map_height_full)
        self.uncorrupted_map = deepcopy(self.game_map)
        self.flood_time_map = np.zeros(
            (self.map_height_full, self.map_width_full))
        self.update_harmony_score()
        self.initial_harmony_score = self.harmony_score

        self.player_pos = deepcopy(self.init_player_pos)   # in order [y, x]
        self.trees = []

        # make sure initial position is valid.
        y, x = self.player_pos
        valid = False
        while not valid:
            tile = self.game_map[y][x]
            valid = True
            if self.character.water_only and not is_water(tile):
                valid = False
            elif not self.character.can_swim and is_water(tile):
                valid = False
            elif not self.character.can_climb and is_mountain(tile):
                valid = False

            if not valid:
                x = random.randint(0, self.map_width_full - 1)
                y = random.randint(0, self.map_height_full - 1)
                self.player_pos = [y, x]

        if self.level > 1:
            if self.level % 4 == 0:
                self.popup_msg("Melkor's rage increases...")
            elif self.level % 2 == 0:
                self.popup_msg("You sense Melkor's power increasing.")

        return False

    def start_game(self, stdscr):
        draw_intro(stdscr)

        while True:
            show_title_screen(stdscr)

            self.character = show_character_selection(stdscr, self)

            # Generate map after character is selected
            self.level = 0
            self.new_level()

            # Create a message window at the bottom of the screen
            msg_win_height = 6
            msg_win_width = curses.COLS
            # H = curses.LINES
            msg_win = curses.newwin(msg_win_height, msg_win_width,
                                    curses.LINES - msg_win_height, 0)

            # Create a character info window on the right side of the screen
            char_info_win_height = curses.LINES - msg_win_height
            char_info_win_width = CHAR_INFO_WIDTH + 2
            char_info_win = curses.newwin(
                char_info_win_height, char_info_win_width, 0, curses.COLS - char_info_win_width)

            self.msg_win = msg_win
            self.char_info_win = char_info_win

            initialize_colors()

            # Refresh windows to display them
            self.draw()
            self.refresh()  # refresh all windows

            should_restart = self.game_loop()

            if not should_restart:
                break     # break out of game loop
