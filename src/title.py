import curses


def show_title_screen(stdscr):
    stdscr.clear()
    title_art = [
        r"         **",
        r"         /\       .   _    +    .   _______  .          .",
        r"        /  \      .  / \     .     /       \      .     +",
        r"       /    \  .    /___\  _  .   /  ^   ^  \         .",
        r"      /  / \ \     /     \       /  ^ ^ ^ ^  \     .",
        r"     /  /   \ \   /  ^  ^ \     / ^ ^ ^ ^ ^ ^ \  .",
        r"    /  /     \ \/  ^ ^ ^  ^\   /  ^  ^  ^  ^  ^\ ",
        r"   /  /       \ \ ^ ^   ^  ^\/ ^  ^  ^   ^  ^  ^\  .",
        r"  /__/_________\_\_______________________________\ ",
        r"                                                          +",
        r"  .       +          .        +       .       +     .        +",
        r"                                                          ",
        r"                       - Shadows Over Arda -              "
    ]
    # Calculate starting row to center the art vertically
    start_row = (curses.LINES - len(title_art)) // 2
    # Calculate starting column to center the art horizontally
    start_col = (curses.COLS - max(len(line) for line in title_art)) // 2
    for i, line in enumerate(title_art):
        stdscr.addstr(start_row + i, start_col, line, curses.A_BOLD)
    stdscr.addstr(start_row + len(title_art) + 1, start_col,
                  "Press any key to continue", curses.A_DIM)
    stdscr.refresh()
    stdscr.getch()  # Wait for any key press
