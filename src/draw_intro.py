import curses
import textwrap


def draw_intro(screen):
    intro_text = """*

*   *

In the time before all, there was Eru, the One, who in the void called forth the Ainur and imparted unto them themes of music divine. Together, they wove a harmony vast and deep, echoing through the emptiness, a prelude to the world that was to be. Yet amongst them, Melkor, mightiest of the Ainur, harbored a desire for dominion and power, and thus he introduced discord into the music, weaving themes of his own imagining, filled with strife and dissonance.

Beholding the tumult, Eru then presented to the Ainur a vision, a revelation of the world they had together sung into being, and with a word, Eru proclaimed "Eä!" -- let these things be! And thus was the world created, brought forth from the Music, a realm of splendor shadowed by the strife sown by Melkor's hand.

Yet Arda, the earth, lay unshaped, a canvas awaiting the touch of the master artists. Thus did the Ainur descend into the world, those who would become the Valar, the Powers of the World, to give form to the land and seas, to ready the way for the coming of Eru’s children -- the Elves, and later, Men.

But Melkor too descended into Arda, filled with envy and malice, seeking to mar all that was made and to claim dominion over the world. So began the first great struggle, the ancient conflict between the Valar and Melkor, a tale of creation and corruption, of light shadowed by darkness. This is the story of those times long past, the saga of Shadows Over Arda.

*   *

*

"""
    screen.clear()
    height, width = screen.getmaxyx()

    paragraphs = intro_text.strip().split('\n\n')  # Split intro text into paragraphs
    wrapped_paragraphs = [textwrap.wrap(
        paragraph, width - 2) for paragraph in paragraphs]

    # Calculate start row to vertically center the text
    total_lines = sum(len(paragraph)
                      for paragraph in wrapped_paragraphs) + len(wrapped_paragraphs) - 1
    start_row = max(0, (height - total_lines) // 2)

    current_row = start_row
    for paragraph in wrapped_paragraphs:
        for line in paragraph:
            # Calculate start column to horizontally center each line
            start_col = (width - len(line)) // 2
            screen.addstr(current_row, start_col, line)
            current_row += 1
        current_row += 1  # Add an empty line between paragraphs

    line = "Press any key to continue"
    start_col = (width - len(line)) // 2
    screen.addstr(current_row, start_col, line, curses.A_DIM)

    screen.refresh()
    screen.getch()  # Wait for user input to proceed
