import math
import random

import noise
import numpy as np


from config import OCEAN_BORDER, VOID_BORDER, TOTAL_BORDER
from display import DISPLAY, MAX_TERRAIN_HEIGHT, HEIGHT_TO_TERRAIN
from melkor import melkor_corrupted
from util import random_sample

PERCENT_WALLS = 0.05
NUM_RIVERS = 3

ob = OCEAN_BORDER
vb = VOID_BORDER
tb = TOTAL_BORDER


def generate_base_map(map_width, map_height):
    # Initialize game map with ocean border
    game_map = [[DISPLAY['void'] if in_void(x, y, map_width, map_height)
                 else (DISPLAY['ocean'] if in_ocean(x, y, map_width, map_height)
                       else ' ') for x in range(map_width + tb * 2)] for y in range(map_height + tb * 2)
                ]
    return game_map


def add_character(game_map, player_pos, character):
    if not character.water_only:
        game_map[player_pos[0]
                 ][player_pos[1]] = ' '  # clear any wall where the player starts


def generate_map(map_width, map_height, player_pos, character):
    # Initialize game map with ocean border
    game_map = generate_base_map(map_width, map_height)

    # Add some walls, avoiding the ocean border
    for _ in range(int(map_height * map_width * PERCENT_WALLS)):
        x = random.randint(0, map_width - 1) + tb
        y = random.randint(0, map_height - 1) + tb
        game_map[y][x] = DISPLAY['wall']

    add_character(game_map, player_pos, character)

    return game_map


def in_void(x, y, map_width, map_height):
    return (x < vb or x >= map_width + vb + 2 * ob
            or y < vb or y >= map_height + vb + 2 * ob)


def in_ocean(x, y, map_width, map_height):
    return ((vb <= x < tb or map_width + tb <= x < map_width + tb + ob) or
            (vb <= y < tb or map_height + tb <= y < map_height + tb + ob))

# Perlin noise, elevation map.


def generate_terrain_and_elevations(map_width, map_height, scale=0.5):
    """Generate game map with landscape terrain, as well as elevation map."""
    base_map = np.zeros((map_height, map_width))
    xscale = scale * map_width
    yscale = scale * map_height

    # Choose a random x and y offset to randomize the noise.
    x_offset = random.randint(0, 100000)
    y_offset = random.randint(0, 100000)

    for y in range(map_height):
        for x in range(map_width):
            base_map[y][x] = noise.pnoise2(
                (x_offset + x) / xscale, (y_offset + y) / yscale, octaves=6)

    # Normalize to [0,1]
    mn = base_map.min()
    mx = base_map.max()
    elevation_map = (base_map - mn) / (mx - mn)

    # Add ocean and void.
    for y in range(map_height):
        for x in range(map_width):
            if in_void(x, y, map_width, map_height):
                elevation_map[y][x] = -10
            if in_ocean(x, y, map_width, map_height):
                elevation_map[y][x] = 0

    game_map = generate_base_map(map_width - 2 * tb, map_height - 2 * tb)
    for y in range(tb, map_height - tb):
        for x in range(tb, map_width - tb):
            game_map[y][x] = get_terrain(elevation_map, x, y)

    # Generate river source locations and then make the rivers.
    river_sources = []
    for _ in range(NUM_RIVERS):
        river_source = find_mountain(game_map)
        river_sources.append(river_source)

    generate_rivers(game_map, elevation_map, river_sources)

    return game_map, elevation_map, river_sources


def get_terrain(elevation_map, x, y):
    terrain_int = min(MAX_TERRAIN_HEIGHT, math.floor(
        elevation_map[y][x] * (MAX_TERRAIN_HEIGHT + 1)))
    terrain = HEIGHT_TO_TERRAIN[terrain_int]
    return DISPLAY[terrain]


def regenerate_terrain_from_elevations(game_map, elevation_map, x_min, x_max, y_min, y_max, retain_melkor=False):
    for y in range(y_min, y_max):
        for x in range(x_min, x_max):
            if retain_melkor and melkor_corrupted(game_map, x, y):
                continue
            game_map[y][x] = get_terrain(elevation_map, x, y)


def generate_rivers(game_map, elevation_map, river_sources):
    """Generate rivers in the game map."""
    for river_source in river_sources:
        x, y = river_source
        generate_river(game_map, elevation_map, x, y)


def find_lower_neighbor(elevation_map, x, y):
    """Find a neighboring cell with a lower elevation to simulate water flow."""
    max_x = len(elevation_map[0])
    max_y = len(elevation_map)
    min_elevation = elevation_map[y][x]
    next_x, next_y = x, y
    for dx, dy in [(-1, 0), (1, 0), (0, -1), (0, 1)]:  # 4-directional movement
        nx, ny = x + dx, y + dy
        if 0 <= nx < max_x and 0 <= ny < max_y and elevation_map[ny][nx] < min_elevation:
            next_x, next_y = nx, ny
            min_elevation = elevation_map[ny][nx]
    return next_x, next_y


def find_mountain(base_map):
    """Find a random cell on the map with a mountain tile."""
    # Collect all mountain cells on the map and then choose one. Returns x,y location pair.
    max_x = len(base_map[0])
    max_y = len(base_map)

    mountain_cells = []
    for y in range(max_y):
        for x in range(max_x):
            if base_map[y][x] == DISPLAY['mountain'] or base_map[y][x] == DISPLAY['hill2']:
                mountain_cells.append((x, y))
    return random_sample(mountain_cells)


def generate_river(base_map, elevation_map, x, y):
    """Generate a river starting from a random pt, flowing downhill."""
    river_path = [(x, y)]  # Initialize river path with the starting point

    # Assume 0 represents the lowest elevation (sea level)
    while True:  # Continue until reaching sea level or another river
        x, y = find_lower_neighbor(elevation_map, x, y)
        if (x, y) in river_path:
            # keep going in prev direction instead.
            if len(river_path) > 1:
                cur_x, cur_y = river_path[-1]
                prev_x, prev_y = river_path[-2]
                dx = cur_x - prev_x
                dy = cur_y - prev_y
                x = cur_x + dx
                y = cur_y + dy
                if (x, y) in river_path:
                    break  # if we still intersected, quit
        river_path.append((x, y))

        # Check for running into another river or ocean.
        if base_map[y][x] == DISPLAY['water'] or base_map[y][x] == DISPLAY['ocean']:
            break

        # Mark the cell as part of the river, assuming -1 indicates water
        base_map[y][x] = DISPLAY['water']

    return river_path
