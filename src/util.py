import logging

import random

logging.basicConfig(filename='log.txt', filemode='w')
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)


def random_sample(list_items):
    return list_items[random.randint(0, len(list_items) - 1)]


def log(msg):
    logger.info(msg)
