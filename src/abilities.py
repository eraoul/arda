
from display import DISPLAY, MIN_LAND_HEIGHT_SCALED, MAX_LAND_HEIGHT_SCALED
import random
import time

from map import in_ocean, in_void, generate_rivers, regenerate_terrain_from_elevations
from melkor import melkor_corrupted
from util import log


ABILITY_MAP = {
    # 'weather': "Control weather",
    'wind': "Blow string winds",
    'wind2': "Call forth rushing gusts",
    'wind3': "Summon mighty tempests",
    'wind4': "Unleash ancient great storms",
    'wind5': "Command the wrath of the Valar's gales",

    'water': "Summon floods",
    'water2': "Call forth expansive floods",
    'water3': "Summon vast, sweeping floods",
    'water4': "Unleash a legendary deluge",
    'water5': "Engulf lands in divine floodwaters",

    'light': "Illuminate dark areas",
    'light2': "Cast beams as bright as Arien's passage",
    'light3': "Brighten lands with dawn's light",
    'light4': "Enkindle expanses like the Two Trees",
    'light5': "Summon the everlasting light of creation",
    # 'barriers': "Create protective barriers",

    'smooth': "Smooth out landscapes, improving harmony",
    'earth': "Shape landscapes, clear corruption",
    'earth2': "Mold broader swathes of land",
    'earth3': "Shift vast tracts of Arda",
    'earth4': "Redefine landscapes with ancient might",
    'earth5': "Craft continents at will, forging new realms",

    'growth': "Accelerate growth",
    'growth2': "Spread verdancy far beyond the simple glade",
    'growth3': "Cover wide lands in lushness untold",
    'growth4': "Summon forests of ancient lore",
    'growth5': "Awaken eternal forests, guardians of life",

    # 'animals': "Control fauna",
    # 'forge': "Create powerful items",
}

animate_sleep_time = 0.2

FLOOD_DURATION = 40  # turns until flood disappears
FLOOD_RECEDE_SCALE = 3  # number of extra turns per flood radius
FLOOD_COST = 2.0  # harmony points per radius

EARTH_COST = 2.0

SMOOTH_COST = 1.0
WIND_COST = 2.0
LIGHT_COST = 2.0
GROWTH_COST = 2.0

GROWTH_MELKOR_PROBABILTY_PER_DIST = 0.8
GROWTH_FOREST_PROBABILTY = 0.2

TREE_EFFECT_RADIUS_MULTIPLIER = 2
TREE_HEALTH_BOOST = 1
TREE_ANIMATION_SLEEP_TIME = 0.03
TREE_GROWTH_TURNS = 10

WIND_SPEED = 3.0
WIND_ANIMATION_DELAY = 0.01
WIND_VARIANCE = 1


def get_ability_radius(ability_key):
    c = ability_key[-1]
    if c == '2':
        return 4
    elif c == '3':
        return 5
    elif c == '4':
        return 8
    elif c == '5':
        return 13
    else:
        return 3


def get_ability_cost(ability_base_cost, radius):
    return ability_base_cost * radius


def has_enough_harmony(character, ability_cost, msg_fn):
    if character.harmony_points < ability_cost:
        msg_fn(
            f"You don't have enough harmony points ({int(ability_cost)}) to use this ability.")
        return False
    return True


def water_ability(character, ability_key, origin_x, origin_y, game_map, elevation_map, flood_time_map, map_width, map_height, map_width_full, map_height_full,
                  msg_fn, draw_fn):
    radius = get_ability_radius(ability_key)
    ability_cost = get_ability_cost(FLOOD_COST, radius)
    if not has_enough_harmony(character, ability_cost, msg_fn):
        return

    cur_terrain = game_map[origin_y][origin_x]
    if cur_terrain != DISPLAY['water'] and cur_terrain != DISPLAY['ocean'] and not character.water_only:
        msg_fn("This ability can only be used near or in water.")
        return
    if character.harmony_points < ability_cost:
        msg_fn(
            f"You don't have enough harmony points ({ability_cost}) to use this ability.")
        return

    to_flood = [(origin_x, origin_y)]
    flooded = set()

    while to_flood and radius > 0:
        x, y = to_flood.pop(0)
        dist = abs(x - origin_x) + abs(y - origin_y)
        if dist > radius:
            continue
        if (x, y) not in flooded:
            flooded.add((x, y))
            tile = game_map[y][x]
            # Don't flood mountains and hills UNLESS they are at our below our current elevation.
            if tile == DISPLAY['mountain'] or tile == DISPLAY['hill1'] or tile == DISPLAY['hill2']:
                # Without the elevation check, Ulmo can get stranded on mountains after cleaning melkor.
                if elevation_map[y][x] > elevation_map[origin_y][origin_x]:
                    continue
            # Don't pollute the ocean
            if tile != DISPLAY['ocean'] and tile != DISPLAY['void']:
                if in_ocean(x, y, map_width, map_height):
                    game_map[y][x] = DISPLAY['ocean']
                else:
                    if melkor_corrupted(game_map, x, y):
                        character.cleaned_corruption()
                    game_map[y][x] = DISPLAY['water']
                    flood_time_map[y][x] = (FLOOD_DURATION -
                                            dist * FLOOD_RECEDE_SCALE)
                draw_fn()  # Refresh the game map to show changes
                # Pause briefly to simulate animation
                time.sleep(animate_sleep_time / radius**2)

            # Add adjacent tiles to the to_flood list
            for dx, dy in [(-1, 0), (1, 0), (0, -1), (0, 1)]:
                nx, ny = x + dx, y + dy
                if 0 <= nx < map_width_full and 0 <= ny < map_height_full and game_map[ny][nx] != DISPLAY['water']:
                    to_flood.append((nx, ny))

    if len(flooded) > 1:
        character.spend_harmony(ability_cost)


def earth_ability(character, ability_key, origin_x, origin_y, direction, game_map, elevation_map, map_width, map_height, map_width_full, map_height_full,
                  river_sources,
                  msg_fn, draw_fn):

    radius = get_ability_radius(ability_key)
    ability_cost = get_ability_cost(EARTH_COST, radius)
    if not has_enough_harmony(character, ability_cost, msg_fn):
        return

    # Define the cone size and effect strength
    cone_size = radius  # 4  # The reach of the ability
    effect_strength = 0.15  # How much to lower or raise the elevation

    # Determine the direction of the cone based on the character's last movement
    # direction: 0 = up, 1 = right, 2 = down, 3 = left
    dx, dy = [(0, -1), (1, 0), (0, 1), (-1, 0)][direction]

    # Calculate the affected tiles
    affected_tiles = []
    for distance in range(1, cone_size + 1):
        for offset in range(-distance, distance + 1):
            if direction % 2 == 0:  # Up or down
                affected_x = origin_x + offset
                affected_y = origin_y + (dy * distance)
            else:  # Right or left
                affected_x = origin_x + (dx * distance)
                affected_y = origin_y + offset

            # Ensure the tile is within bounds
            if 0 <= affected_x < map_width_full and 0 <= affected_y < map_height_full:
                if in_void(affected_x, affected_y, map_width, map_height):
                    continue
                if in_ocean(affected_x, affected_y, map_width, map_height):
                    continue
                affected_tiles.append((affected_x, affected_y))

    # Modify the elevations
    for x, y in affected_tiles:
        if melkor_corrupted(game_map, x, y):
            character.cleaned_corruption()

        current_elevation = elevation_map[y][x]
        # Decrease elevation near the character, increase it further away
        distance = abs(x - origin_x) + abs(y - origin_y)
        if distance <= cone_size // 2:
            elevation_map[y][x] = max(
                MIN_LAND_HEIGHT_SCALED, current_elevation - effect_strength)
        else:
            elevation_map[y][x] = min(
                MAX_LAND_HEIGHT_SCALED, current_elevation + effect_strength)

    # find min,max x,y of affected tiles.
    if not affected_tiles:
        return

    min_x = min([x for x, _ in affected_tiles])
    max_x = max([x for x, _ in affected_tiles])
    min_y = min([y for _, y in affected_tiles])
    max_y = max([y for _, y in affected_tiles])

    # Regenerate terrain and rivers based on the new elevation map
    regen_map_and_rivers(game_map, elevation_map,
                         river_sources, min_x, max_x, min_y, max_y, draw_fn)

    # Spend harmony points.
    character.spend_harmony(ability_cost)

    msg_fn(f"The earth shifts and reshapes at {character.name}'s command.")


def regen_map_and_rivers(game_map, elevation_map, river_sources, min_x, max_x, min_y, max_y,
                         draw_fn, retain_melkor=False):
    # Regenerate terrain and rivers based on the new elevation map
    regenerate_terrain_from_elevations(game_map, elevation_map, min_x,
                                       max_x + 1, min_y, max_y + 1, retain_melkor=retain_melkor)
    # Redraw the map
    time.sleep(animate_sleep_time)
    draw_fn()

    generate_rivers(game_map, elevation_map, river_sources)


def smooth_ability(character, ability_key, origin_x, origin_y, game_map, elevation_map, map_width, map_height, map_width_full, map_height_full,
                   river_sources, msg_fn, draw_fn):
    # Increase radius based on char level.
    lvl = min(4, character.level // 5)
    radius = get_ability_radius(str(lvl))
    # keep smooth cost constant as if radius 3
    ability_cost = get_ability_cost(SMOOTH_COST, 3)
    if not has_enough_harmony(character, ability_cost, msg_fn):
        return

    # Calculate the affected tiles based on the radius
    affected_tiles = [(x, y) for x in range(max(0, origin_x - radius), min(map_width, origin_x + radius + 1))
                      for y in range(max(0, origin_y - radius), min(map_height, origin_y + radius + 1))]

    # Create a copy of the elevation map to store the new elevations
    new_elevations = [[elevation_map[y][x]
                       for x in range(map_width_full)] for y in range(map_height_full)]

    min_x = float('inf')
    min_y = float('inf')
    max_x = -float('inf')
    max_y = -float('inf')

    for x, y in affected_tiles:
        # Skip tiles outside of cirlce or in void/ocean.
        dist = (x - origin_x) ** 2 + (y - origin_y) ** 2
        if dist > radius**2:
            continue
        if in_void(x, y, map_width, map_height):
            continue
        if in_ocean(x, y, map_width, map_height):
            continue

        min_x = min(min_x, x)
        min_y = min(min_y, y)
        max_x = max(max_x, x)
        max_y = max(max_y, y)

        # Calculate the average elevation for the tile and its immediate neighbors
        total_elevation = 0
        count = 0
        for dx in [-1, 0, 1]:
            for dy in [-1, 0, 1]:
                nx, ny = x + dx, y + dy
                if 0 <= nx < map_width_full and 0 <= ny < map_height_full:
                    total_elevation += elevation_map[ny][nx]
                    count += 1

        if count > 0:
            new_elevations[y][x] = total_elevation / count

    # Update the elevation map with the smoothed elevations
    for x, y in affected_tiles:
        elevation_map[y][x] = new_elevations[y][x]

    # Update terrain types and rivers based on the new elevation values.
    regen_map_and_rivers(game_map, elevation_map,
                         river_sources, min_x, max_x, min_y, max_y, draw_fn, retain_melkor=True)

    # Spend harmony points or implement other ability costs as needed
    character.spend_harmony(ability_cost)

    msg_fn("The land softens and settles.")


def growth_ability(character, ability_key, origin_x, origin_y, game_map, map_width, map_height, map_width_full, map_height_full, msg_fn, draw_fn):
    radius = get_ability_radius(ability_key)
    ability_cost = get_ability_cost(GROWTH_COST, radius)

    if not has_enough_harmony(character, ability_cost, msg_fn):
        return

    corrupted_str = ''
    for dy in range(-radius, radius + 1):
        for dx in range(-radius, radius + 1):
            x, y = origin_x + dx, origin_y + dy
            if (0 <= x < map_width_full and 0 <= y < map_height_full
                and not in_ocean(x, y, map_width, map_height)
                    and not in_void(x, y, map_width, map_height)):
                # x% chance of transforming land into forest
                if melkor_corrupted(game_map, x, y):
                    dist = (dy**2 + dx**2)**0.5
                    if random.random() < GROWTH_MELKOR_PROBABILTY_PER_DIST**dist:
                        character.cleaned_corruption()
                        game_map[y][x] = DISPLAY['forest']
                        corrupted_str = 'corrupted '
                elif random.random() < GROWTH_FOREST_PROBABILTY:
                    game_map[y][x] = DISPLAY['forest']

    character.spend_harmony(ability_cost)
    draw_fn()
    msg_fn(f"Forests grow, purifying the {corrupted_str}land.")


def wind_ability(character, ability_key, origin_x, origin_y, direction, game_map, uncorrupted_map,
                 map_width, map_height, map_width_full, map_height_full, msg_fn, draw_fn):
    radius = get_ability_radius(ability_key)
    ability_cost = get_ability_cost(WIND_COST, radius)

    if not has_enough_harmony(character, ability_cost, msg_fn):
        return

    # direction: 0 = up, 1 = right, 2 = down, 3 = left
    if direction == 0:
        dxw = 0
        dyw = -1
    elif direction == 1:
        dxw = 1
        dyw = 0
    elif direction == 2:
        dxw = 0
        dyw = 1
    elif direction == 3:
        dxw = -1
        dyw = 0
    else:
        raise ValueError(f"Invalid direction: {direction}")

    corruption_present = False

    new_corruptions = []

    for dy in range(-radius, radius + 1):
        for dx in range(-radius, radius + 1):
            x, y = origin_x + dx, origin_y + dy
            if 0 <= x < map_width_full and 0 <= y < map_height_full and melkor_corrupted(game_map, x, y):
                # Assume logic to determine direction, could be random or based on character orientation
                # Logic for new position after blowing corruption
                corruption_present = True
                nx, ny = x + int(dxw * WIND_SPEED), y + int(dyw * WIND_SPEED)
                nx += random.randint(-WIND_VARIANCE, WIND_VARIANCE)
                ny += random.randint(-WIND_VARIANCE, WIND_VARIANCE)
                if (0 <= nx < map_width_full and 0 <= ny < map_height_full
                        and not in_void(nx, ny, map_width, map_height)):
                    # Move corruption to new position
                    new_corruptions.append((ny, nx, game_map[y][x]))
                else:
                    # Called when corruption is blown into the void or off the edge
                    character.cleaned_corruption()

                # Revert original tile to its previous state
                game_map[y][x] = uncorrupted_map[y][x]
                time.sleep(WIND_ANIMATION_DELAY)
                draw_fn()

    # move corruptions to new spots
    for ny, nx, tile in new_corruptions:
        # log(f"Moving corruption {tile} to ({nx}, {ny})")
        game_map[ny][nx] = tile
        time.sleep(WIND_ANIMATION_DELAY)
        draw_fn()

    character.spend_harmony(ability_cost)
    # draw_fn()

    if corruption_present:
        msg_fn("Winds blow, scattering the dark blight.")
    else:
        msg_fn("Winds blow across Arda.")


def light_ability(character, ability_key, origin_x, origin_y, game_map, trees, msg_fn, draw_fn):
    radius = get_ability_radius(ability_key)
    ability_cost = get_ability_cost(LIGHT_COST, radius)

    if not has_enough_harmony(character, ability_cost, msg_fn):
        return

    # Place the initial tree with starting radius 0 (will grow)
    # Assuming 'trees' is a list tracking all trees
    trees.append({'x': origin_x, 'y': origin_y,
                 'radius': 0, 'max_radius': radius})
    game_map[origin_y][origin_x] = DISPLAY['light_tree']

    character.spend_harmony(ability_cost)
    draw_fn()
    msg_fn("A radiant tree sprouts, beginning to spread its light.")


def apply_tree_effects(character, turns, origin_x, origin_y, game_map, trees, map_width, map_height, map_width_full, map_height_full,
                       uncorrupted_map, draw_fn, msg_fn):
    for tree in trees:
        if tree['radius'] < tree['max_radius'] and turns % TREE_GROWTH_TURNS == 0:
            tree['radius'] += 1  # Increase the tree's radius every N turns
            # draw the tree with the new radius.
            radius = tree['radius']
            for dy in range(-radius, radius + 1):
                for dx in range(-radius, radius + 1):
                    if dx**2 + dy**2 > radius**2:
                        continue
                    x, y = tree['x'] + dx, tree['y'] + dy
                    if (0 <= x < map_width_full and 0 <= y < map_height_full
                        and not in_ocean(x, y, map_width, map_height)
                            and not in_void(x, y, map_width, map_height)):
                        game_map[y][x] = DISPLAY['light_tree']
                        draw_fn()
                        time.sleep(TREE_ANIMATION_SLEEP_TIME)

        effect_radius = tree['radius'] * TREE_EFFECT_RADIUS_MULTIPLIER
        log(f"Effect radius: {effect_radius}")
        # Apply tree's effects within its current corruption- and health-effect radius
        for dy in range(-effect_radius, effect_radius + 1):
            for dx in range(-effect_radius, effect_radius + 1):
                x, y = tree['x'] + dx, tree['y'] + dy
                # log('checking tile at (%d, %d)' % (x, y))
                if (0 <= x < map_width_full and 0 <= y < map_height_full
                        and dx**2 + dy**2 <= effect_radius**2):
                    # and not in_ocean(x, y, map_width, map_height)
                    #     and not in_void(x, y, map_width, map_height)):
                    if melkor_corrupted(game_map, x, y):
                        # log(f"tile near tree at ({x}, {y}) is corrupted.")
                        character.cleaned_corruption()
                        # Revert to its previous state
                        game_map[y][x] = uncorrupted_map[y][x]

                    # Check for character health boost if within tree's radius
                    if (x, y) == (origin_x, origin_y) and character.health < character.max_health:
                        character.increase_health(TREE_HEALTH_BOOST)
                        msg_fn(f"{character.name} is healed by the Tree.")

    draw_fn()
